<?php

/**
 * 361GRAD Element List
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_list'] =
    '{type_legend},type,headline,dse_subheadline,dse_secondline;' .
    '{list_legend},dse_listheadline,dse_listtext;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondline'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'maxlength' => 200,
        'tl_class'  => 'clr'
    ],
    'sql'       => 'varchar(255) NOT NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_subheadline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'mandatory' => false,
        'maxlength' => 200,
        'tl_class'  => 'w50'
    ],
    'sql'       => 'varchar(255) NOT NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_listheadline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_listheadline'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_listtext'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_listtext'],
    'inputType' => 'textarea',
    'eval'      => [
        'tl_class' => 'clr',
        'rte'      => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
